# docker-compose presentation

This presentation assumes you're able to run docker commands on your local machine. 
If you're using OS X or Windows, install [boot2docker](http://boot2docker.io/).

Because we're using [docker-compose](https://docs.docker.com/compose/install/), you'll also want to have that available 


Run `rails new` inside the container

`docker-compose run web rails new . --force --database=postgresql --skip-bundle`

Alter the bundle: Enable js runtime on line 15(?) of Gemfile

Replace config/database.yml, with next lines:

```
development: &default
  adapter: postgresql
  encoding: unicode
  database: postgres
  pool: 5
  username: postgres
  password:
  host: db

test:
  <<: *default
  database: myapp_test
```

Rebuild the container; we've altered the bundle

`docker-compose build`

Start the app with

`docker-compose up`

Start another shell, and add a scaffold through docker compose:

`docker-compose run web   rails g scaffold user name:string bio:text`

Migrate the database through docker compose:

`docker-compose run web   rails g scaffold user name:string bio:text`

Go check your product on http://localhost:3000/

If you're using boot2docker, the command `boot2docker ip` will inform of the IP address you should be using instead of localhost